<!--
	Author: Tarlan Ismayilsoy
	Note: some code pieces, such as the style, and JS script were
	taken from https://codepen.io/hckkiu/pen/KKzgEMr and modified.
-->

<html>
	<head>
		<title>Meal reminder</title>
		<style> 
        	html, body {
			  width: 100%;
			  height: 100%;
			  background-color: #3a3a3a;
			}
			.container {
			  display: flex;
			  align-items: center;
			  width: 100%;
			  height: 100%;
			}
			.typewriter {
			  font-family: sans-serif;
			  color: #fff;
			  padding-left: 30px;
			  display: block;
			}
			.typewriter-text {
			  padding-right: 10px;
			  color: #ffe509;
			  border-right: solid #ffe509 7px;
			  text-transform: uppercase;
			  animation: cursor 1s ease-in-out infinite;
			  font-weight: bold;
			}
			@keyframes cursor {
			  from { border-color: #ffe509; }
			  to { border-color: transparent; }
			}

			@media (max-width: 767.98px) {
			  .typewriter { font-size: 35px; }
			}
			@media (min-width: 768px) {
			  .typewriter { font-size: 60px; }
			}
    	</style>
	</head> 

	<body>
		<%@ page import = "java.util.*" %>
		<%@ page import = "java.text.SimpleDateFormat" %>

		<%
			Date currentDate = new Date();
			SimpleDateFormat localDateFormat = new SimpleDateFormat("HH");
			int time = new Integer(localDateFormat.format(currentDate));
		%>

		<div class='container'>
			<p class='typewriter'>It is
				<span id = "typewriter-txt" class = "typewriter-text" data-text = '
					<%
						if (time < 12)
						{
							out.print("breakfast");
						}
						else if(time <= 16) // time between 12 and 16
						{
							out.print("lunch");
						}
						else // time more than 16
						{
							out.print("dinner");
						}
					%>
				'>
			</span>time
			</p>
		</div>

		<script> // This script is just for creating a cool typewriter animation. Main logic is written above in Java.
			window.onload = function init()
			{  
				var typewriterText = document.getElementById("typewriter-txt")
				type(0, typewriterText.getAttribute('data-text'))

				function type( index, text )
				{
					var textIndex = 1;
					var tmp = setInterval(function() {
								typewriterText.innerHTML = text.substr( index, textIndex );
								textIndex++;
						}, 150);
				}
			}
		</script>
	</body>
</html>